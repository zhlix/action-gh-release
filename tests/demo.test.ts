import { paths, parseConfig, isTag, unmatchedPatterns, uploadUrl, Config } from '../src/util'
import { release, upload, GitHubReleaser } from '../src/github'
import { getOctokit } from '@actions/github'

const main = async () => {
  const config: Config = {
    act: true,
    github_repository: 'zhanglixin/test',
    github_token: '4c717526c5e992886910a19885845b0146475d6e',
  } as any

  const gh = getOctokit(config.github_token, {
    //new oktokit(
    throttle: {
      onRateLimit: (retryAfter, options) => {
        console.warn(`Request quota exhausted for request ${options.method} ${options.url}`)
        if (options.request.retryCount === 0) {
          // only retries once
          console.log(`Retrying after ${retryAfter} seconds!`)
          return true
        }
      },
      onAbuseLimit: (retryAfter, options) => {
        // does not retry, only logs a warning
        console.warn(`Abuse detected for request ${options.method} ${options.url}`)
      },
    },
  })

  await upload(
    config as any,
    gh,
    // @ts-ignore
    // uploadUrl(rel.upload_url ?? `${rel.url}/assets`),
    // 'http://127.0.0.1:7001/api/v1/repos/zhanglixin/test/releases/9/assets',
    'http://192.168.1.106:7200/api/v1/repos/zhanglixin/test/releases/9/assets',
    'tests/data/foo/bar.txt',
    [],
  )
}

main()
